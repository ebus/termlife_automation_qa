package com.qa.testcases;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AcceleratedQualifiersPageAction;
import com.qa.pageActions.ApplyESignatureSubmitAction;
import com.qa.pageActions.BeneficiaryInfoPageAction;
import com.qa.pageActions.CaseInformationPageAction;
import com.qa.pageActions.ChildrenBenefitRiderAction;
import com.qa.pageActions.ExistingInsuranceAction;
import com.qa.pageActions.FamilyHistoryPageAction;
import com.qa.pageActions.FinancialSupplementAction;
import com.qa.pageActions.FinancialSupplementContAction;
import com.qa.pageActions.HIVConsentAction;
import com.qa.pageActions.HealthQuestionsChildrenPageAction;
import com.qa.pageActions.HealthQuestionsContPageAction;
import com.qa.pageActions.HealthQuestionsPageAction;
import com.qa.pageActions.Illustration_PolicyPageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.MultipleOwnersPageAction;
import com.qa.pageActions.OwnerInfoContJuvenilePageAction;
import com.qa.pageActions.OwnerInfoContPageAction;
import com.qa.pageActions.OwnerInformationJuvenilePageAction;
import com.qa.pageActions.OwnerInformationPageAction;
import com.qa.pageActions.PersonalInformationAction;
import com.qa.pageActions.PremiumInformationAction;
import com.qa.pageActions.PremiumPayorInfoPageAction;
import com.qa.pageActions.ProposedInsuredInfoAction;
import com.qa.pageActions.RepresentativeInformationAction;
import com.qa.pageActions.SignatureMethodAction;
import com.qa.pageActions.TemporaryInsuranceAgreementAction;
import com.qa.pageActions.TermLifeInsuranceCoverageRidersAction;
import com.qa.pageActions.ThirdPartyOptionPageAction;
import com.qa.pageActions.USAPatriotActAction;
import com.qa.pageActions.ValidateAndLockDataAction;
import com.qa.pageActions.eSigDisclosuresAction;
import com.qa.pageActions.eSignatureConsentAction;
import com.qa.pageActions.eSignaturePartiesAction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OATermLifeTest extends TestBase {

	LoginPageAction loginPageAction;
	CaseInformationPageAction caseInformationPageAction;
	ProposedInsuredInfoAction proposedInsuredInfoAction;
	Illustration_PolicyPageAction illustration_PolicyPageAction;
	OwnerInformationPageAction ownerInformationPageAction;
	OwnerInformationJuvenilePageAction ownerInfoJuvenilePageAction;
	OwnerInfoContPageAction ownerInfoContPageAction;
	OwnerInfoContJuvenilePageAction ownerInfoContJuvenilePageAction;
	MultipleOwnersPageAction multipleOwnersPageAction;
	PremiumPayorInfoPageAction premiumPayorInfoPageAction;
	BeneficiaryInfoPageAction beneficiaryInfoPageAction;
	TermLifeInsuranceCoverageRidersAction termLifeInsuranceCoverageRidersAction;
	AcceleratedQualifiersPageAction acceleratedQualifiersPageAction;
	ChildrenBenefitRiderAction childrenBenefitRiderAction;
	PersonalInformationAction personalInformationAction;
	HealthQuestionsPageAction healthQuestionsPageAction;
	HealthQuestionsContPageAction healthQueContPageAction;
	HealthQuestionsChildrenPageAction healthQuestionsChildrenPageAction;
	FamilyHistoryPageAction familyHistoryPageAction;
	HIVConsentAction HIVconsentAction;
	ExistingInsuranceAction existingInsuranceAction;
	PremiumInformationAction premiumInformationAction;
	USAPatriotActAction usaPatriotActAction;
	FinancialSupplementAction financialSupplementAction;
	FinancialSupplementContAction financialSupplementContAction;
	TemporaryInsuranceAgreementAction temporaryInsuranceAgreementAction;
	ThirdPartyOptionPageAction thirdPartyOptionPageAction;
	ValidateAndLockDataAction validateAndLockDataAction;
	SignatureMethodAction signatureMethodAction;
	eSigDisclosuresAction eSigdiclosuresAction;
	eSignatureConsentAction eSignatureconsentAction;
	eSignaturePartiesAction eSignaturepartiesAction;
	RepresentativeInformationAction representativeInformationAction;
	ApplyESignatureSubmitAction applyESignatureSubmitAction;
	
	int iteration = 0;

	public OATermLifeTest() {
		super();
	}

	@DataProvider
	public Object[][] getApplicationData() {
		Object data[][] = TestUtil.getTestData("Application");
		return data;
	}



	@Test(priority = 1, dataProvider = "getApplicationData")
	public void TermLifeAppTest(String scenarioName, String indExecute, String userId, String pwd, String firstName, 
			String middleName, String lastName,String dob, String gender, String state, String productType,
			String product, String illustration, 
			String useUnisexRate, String primaryInsuredClass, String permanentFlatExtra, String temporaryFlatExtra, String forYears, String Plan_Illustration, String inputMethods, String BaseFaceAmount_Illustration, String PremiumAmount_Illustration,
			String Payment_mode, String WPD_illustration,String WPD_duration, String waiver_conversion_option,String CBR_illustration, String CBR_Unit,String CBR_duration,

			String SSN, String birthCountry, 	
			String birthState, String maritalQuestion, String Street, String City, String ZipCode, 
			String County, String yearsAtAddress, String PhoneNumber, String Email, String willTheProposedInsuredBeOwner, String whoWillBePayor, 
			
			String payorRelationship, String thirdPartyflag, String payorEntityName,
			String payorFirstName, String payorLastName, String payorStreet, String payorCity, String payorState,
			String payorZIP, String payorCountry, String payorYrsAtAdd, String payorSSN, String payorDOB, 
			String payorBirthState,
			
			String applicationSignedBy, String indLegalGuardianPolicyOwner, String firstName_LG, String lastName_LG,
			String SSN_LG,
			
			String DoesInsuredHaveDL, String DLNo, String IssueState, String ExpirationDate, String earned,
			String unEarned, String netWorth, String isProposedInsuredUSCitizen, String countryofCitizenship, String doesProposedInsuredHoldGreenCard,	
			String greenCardNumber, String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, 	
			String visaNumber, String provideDetails, String typeOwner, String relnProposedIns, String ownerFirstName, 
			String ownerLastName, String ownerSSN, String strDOB, String ownerBirthState, String ownerGender,					
			
			String ownerMaritalStatus, String ownerDL_State, String strDLExpDate, String isMultipleOwner, String mulOwnerFirst,
			String mulOwnerLast,  String mOwner_Street, String mOwner_city, String mOwner_State, String mOwner_ZIPCode,
			String mOwner_DOB, String mOwner_SSN,  String numOfPB,
			String strShareIndicator, String strDeceasedSharePaid, String strRelationshipBene1, String strFirstNameBene1, String strLastNameBene1,
			String strGenderBene1, String strSSNBene1, String strSharePerBene1, String strEntityNameBene1, String strCorporateOfficer_Bene1, 
			String strTitle_Bene1, String strStateIncorporation_Bene1, String strOtherRelation_Bene1, String strRelationshipBene2, String strFirstNameBene2, 
			String strLastNameBene2, String strGenderBene2, String strSSNBene2, String strSharePerBene2, String strEntityNameBene2, 				
			String strOtherRelation_Bene2, String strRelationshipBene3, String strFirstNameBene3, String strLastNameBene3, String strGenderBene3,
			String strSSNBene3, String strSharePerBene3, String strEntityNameBene3, String strOtherRelation_Bene3, String strRelationshipBene4, 
			String strFirstNameBene4, String strLastNameBene4, String strGenderBene4, String strSSNBene4, String strSharePerBene4, 
			String strEntityNameBene4, String strOtherRelation_Bene4, String strRelationshipBene5, String strFirstNameBene5, String strLastNameBene5,
			String strGenderBene5, String strSSNBene5, String strSharePerBene5, String strEntityNameBene5, String strOtherRelation_Bene5, 			
			String FaceAmount, String Plan, String CashWithApp, String PremiumAmount, String wpdRider,
			String childrenbenefitRider,String Unit,String Other,String Othervalue,String Completeformvalue,String concurrentcoverage,String amountexceed,
			String child_firstname,String child_lastname,String child_relationship,String child_gender,String child_height_ft,String child_height_in,String child_weight,
			String child_dob,String child_all_listed,String AUW_Ques1,String AUW_Ques2, String AUW_Ques3,
			String paymentMode, String otherMode,  
			String isAUW, String tobacco_Uses,
			String isCompletingHealthInformation, String isExistingInsurance, String isPolicyReplacing, 
			String is1035Exchange, String isAdditionalRepresentatives, String AddRepresentativesCount, String representativesName1,
			String sharePercentage1,String license_Identification_No1, String representativesCode1, String representativesName2, String sharePercentage2, String license_Identification_No2, String representativesCode2,
			String representativesName3, String sharePercentage3,String license_Identification_No3, String representativesCode3, String representativesName4, String sharePercentage4, String license_Identification_No4,
			String representativesCode4,String Salary_currentyear,String Salary_lastyr,String Salary_2yrsago,String Pension_currentyear,String Pension_lastyr,String Pension_2yrsago,String Bonus_currentyear,
			String Bonus_lastyr,String Bonus_2yrsago,String Earnings_currentyear,String Earnings_lastyr,String Earnings_2yrsago,String Deduction_currentyear,String Deductyion_lastyr,
			String Deduction_2yrsago,String Dividends,String Interest,String NetCapGain,String Rentalincome,String other,String Cash_savings,String Interest_Business,
			String Personal_Property,String Real_Estate,String Other_Real_Estate,String Other_networth,String Purpose_Insurance,String signatureparties_state) {

		
		
		extentTest = extent.startTest("IllustrationTest - " + scenarioName);
		if (indExecute.equalsIgnoreCase("No")) {
			extentTest.log(LogStatus.INFO, "Execute column is No in the data sheet");
			throw new SkipException(scenarioName + " is Skipped");
		}
		
			
			try {
				Thread.sleep(3000);
				
				intialization();

				extentTest.log(LogStatus.INFO, "Browser used: " + prop.getProperty("browser"));
				extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
				
				loginPageAction = new LoginPageAction();
				caseInformationPageAction = loginPageAction.logIn(userId, pwd);
				illustration_PolicyPageAction = caseInformationPageAction.selectProductIllustration(firstName, lastName, dob, gender,
						state, productType, product, illustration);
				
				proposedInsuredInfoAction = illustration_PolicyPageAction.enterDataPolicyPage(gender,illustration,
						useUnisexRate,primaryInsuredClass, permanentFlatExtra, temporaryFlatExtra, forYears, Plan_Illustration, inputMethods, 
						BaseFaceAmount_Illustration, PremiumAmount_Illustration,Payment_mode, WPD_illustration, WPD_duration, waiver_conversion_option, 
						CBR_illustration, CBR_Unit, CBR_duration);
				
				ownerInformationPageAction = proposedInsuredInfoAction.enterData(illustration, state, 
						middleName, SSN, gender, birthCountry, birthState, 
						maritalQuestion, Street, City, ZipCode, County, yearsAtAddress, PhoneNumber, Email,
						willTheProposedInsuredBeOwner, whoWillBePayor,thirdPartyflag, applicationSignedBy, indLegalGuardianPolicyOwner, 
						firstName_LG, lastName_LG, SSN_LG, DoesInsuredHaveDL, DLNo, IssueState, ExpirationDate, 
						earned, unEarned, netWorth, isProposedInsuredUSCitizen, countryofCitizenship, 
						doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate, doesProposedInsuredHoldUSVisa, 
						typeOfVisa, visaExpirationDate, visaNumber, provideDetails);
				
				ownerInfoJuvenilePageAction = ownerInformationPageAction.enterOwnerInformationData(state, willTheProposedInsuredBeOwner, typeOwner, relnProposedIns,
						ownerFirstName, ownerLastName, ownerSSN, strDOB, ownerBirthState, ownerGender, 
						ownerMaritalStatus, ownerDL_State, strDLExpDate, isMultipleOwner);
				
				ownerInfoContPageAction = ownerInfoJuvenilePageAction.enterOwnerInformationData(applicationSignedBy, 
						indLegalGuardianPolicyOwner, state, 
						willTheProposedInsuredBeOwner, typeOwner, relnProposedIns, ownerFirstName, ownerLastName, ownerSSN,
						strDOB, ownerBirthState, ownerGender, ownerMaritalStatus, ownerDL_State, strDLExpDate, 
						isMultipleOwner);
				
				ownerInfoContJuvenilePageAction = ownerInfoContPageAction.enterOwnerinfoCont(willTheProposedInsuredBeOwner, 
						typeOwner);
				
				multipleOwnersPageAction = ownerInfoContJuvenilePageAction.enterOwnerinfoCont(typeOwner);
				
				premiumPayorInfoPageAction = multipleOwnersPageAction.enterMultipleOwners(willTheProposedInsuredBeOwner, isMultipleOwner, mulOwnerFirst, mulOwnerLast, 
						mOwner_Street, mOwner_city, mOwner_State, mOwner_ZIPCode, mOwner_DOB, mOwner_SSN);
				
				beneficiaryInfoPageAction = premiumPayorInfoPageAction.enterPremiumPayorInfo(whoWillBePayor, payorRelationship, 
						payorEntityName, payorFirstName, payorLastName, payorStreet, payorCity, payorState, payorZIP, 
						payorCountry, payorYrsAtAdd, payorSSN, payorDOB, payorBirthState);
				
				termLifeInsuranceCoverageRidersAction = beneficiaryInfoPageAction.detailsOfPrimaryBenef(numOfPB, strShareIndicator, strDeceasedSharePaid, strRelationshipBene1, 
						strFirstNameBene1, strLastNameBene1, strGenderBene1, strSSNBene1, strSharePerBene1, strEntityNameBene1, strCorporateOfficer_Bene1, 
						strTitle_Bene1, strStateIncorporation_Bene1, strOtherRelation_Bene1, strRelationshipBene2, strFirstNameBene2, strLastNameBene2, strGenderBene2, 
						strSSNBene2, strSharePerBene2, strEntityNameBene2, strOtherRelation_Bene2, strRelationshipBene3, strFirstNameBene3, strLastNameBene3, strGenderBene3, 
						strSSNBene3, strSharePerBene3, strEntityNameBene3, strOtherRelation_Bene3, strRelationshipBene4, strFirstNameBene4, strLastNameBene4, strGenderBene4, 
						strSSNBene4, strSharePerBene4, strEntityNameBene4, strOtherRelation_Bene4, strRelationshipBene5, strFirstNameBene5, strLastNameBene5, strGenderBene5, 
						strSSNBene5, strSharePerBene5, strEntityNameBene5, strOtherRelation_Bene5);
					
				childrenBenefitRiderAction = termLifeInsuranceCoverageRidersAction.enterDataTermLifeInsuranceCoverageRiders(illustration,BaseFaceAmount_Illustration,FaceAmount,Plan,CashWithApp,PremiumAmount,wpdRider,childrenbenefitRider,Unit,Other,Othervalue,Completeformvalue,concurrentcoverage,amountexceed);

				acceleratedQualifiersPageAction = childrenBenefitRiderAction.detailsOfChildrenBenefitRider(child_firstname,child_lastname,child_relationship,child_gender,child_height_ft,child_height_in,child_weight,child_dob,child_all_listed);	
				
				personalInformationAction = acceleratedQualifiersPageAction.enterAUWData(illustration,BaseFaceAmount_Illustration,FaceAmount,isAUW, AUW_Ques1, AUW_Ques2, AUW_Ques3);
				
				healthQuestionsPageAction = personalInformationAction.enterDataPersonalInformation(tobacco_Uses, isCompletingHealthInformation);
			
				healthQueContPageAction = healthQuestionsPageAction.enterHealthQuestions(isCompletingHealthInformation);
				
				healthQuestionsChildrenPageAction = healthQueContPageAction.enterHealthQueCont(isCompletingHealthInformation);
				
				familyHistoryPageAction = healthQuestionsChildrenPageAction.detailsOfChildrenHealthQuestion();
				
				HIVconsentAction = familyHistoryPageAction.enterFamilyHistory(isCompletingHealthInformation);
				
				existingInsuranceAction = HIVconsentAction.enterDataHIVConsent(state);
				
				premiumInformationAction = existingInsuranceAction.enterDataExistingInsurance(state,isExistingInsurance, 
						isPolicyReplacing, is1035Exchange);
				
				usaPatriotActAction = premiumInformationAction.enterDataPremiumInformation(illustration,paymentMode, otherMode);
				
				financialSupplementAction = usaPatriotActAction.enterDataUSAPatriotAct(willTheProposedInsuredBeOwner, 
						isMultipleOwner);
				
				financialSupplementContAction = financialSupplementAction.enterDataFinancialSupplement(illustration,BaseFaceAmount_Illustration,FaceAmount,Salary_currentyear,Salary_lastyr,Salary_2yrsago,Pension_currentyear,Pension_lastyr,Pension_2yrsago,Bonus_currentyear,Bonus_lastyr,Bonus_2yrsago,
						Earnings_currentyear,Earnings_lastyr,Earnings_2yrsago,Deduction_currentyear,Deductyion_lastyr,Deduction_2yrsago,Dividends,Interest,NetCapGain,Rentalincome,
						other,Cash_savings,Interest_Business,Personal_Property,Real_Estate,Other_Real_Estate,Other_networth,Purpose_Insurance); 
				
				temporaryInsuranceAgreementAction = financialSupplementContAction.enterDataFinancialSupplementCont(illustration,BaseFaceAmount_Illustration,FaceAmount); 
				
				thirdPartyOptionPageAction = temporaryInsuranceAgreementAction.enterDataTempInsAgreement(CashWithApp);
				
				validateAndLockDataAction = thirdPartyOptionPageAction.enterThirdPartyOption(state);
				                                     
				signatureMethodAction = validateAndLockDataAction.enterDataValAndLockData();
				
				eSigdiclosuresAction = signatureMethodAction.enterDataSignatureMethod();
				
				eSignatureconsentAction = eSigdiclosuresAction.enterDataESigDisclosures(willTheProposedInsuredBeOwner, 
						isMultipleOwner, whoWillBePayor, indLegalGuardianPolicyOwner);
				
				eSignaturepartiesAction = eSignatureconsentAction.enterDataESignatureConsent(willTheProposedInsuredBeOwner, 
						isMultipleOwner, whoWillBePayor, indLegalGuardianPolicyOwner);
				
				representativeInformationAction = eSignaturepartiesAction.enterDataeSignatureParties(willTheProposedInsuredBeOwner, 
						isMultipleOwner, whoWillBePayor, indLegalGuardianPolicyOwner,signatureparties_state);
				
				applyESignatureSubmitAction = representativeInformationAction.enterDataRepresentativeInfo(state,isAdditionalRepresentatives, 
						AddRepresentativesCount, representativesName1,sharePercentage1,license_Identification_No1,
						 representativesCode1, representativesName2, sharePercentage2, license_Identification_No2,  
						representativesCode2, representativesName3, sharePercentage3,license_Identification_No3,  representativesCode3, representativesName4, sharePercentage4,license_Identification_No4, representativesCode4);
					
				applyESignatureSubmitAction.enterApplyESignatureSubmit(state,City);
			} catch (Exception e) {
				e.printStackTrace();
				// genericFunction.takeScreenshot("Error");
			} finally {

			}
		}
	}

