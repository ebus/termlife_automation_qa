package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;

import com.qa.pages.Illustration_PolicyPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.CaseInformationPageAction.age;


public class Illustration_PolicyPageAction extends Illustration_PolicyPage{
	
	public Illustration_PolicyPageAction() {
		super();
	}
	
	public static boolean childbenefitRider_illustration_flag;
	//Action
	public ProposedInsuredInfoAction enterDataPolicyPage(String gender,String illustration,String useUnisexRate, String primaryInsuredClass,
			String permanentFlatExtra, String temporaryFlatExtra, String forYears, String Plan_Illustration, String inputMethods,
			String BaseFaceAmount_Illustration, String PremiumAmount_Illustration,String Payment_mode,String WPD_illustration,String WPD_duration,
			String waiver_conversion_option,String CBR_illustration,String CBR_Unit,String CBR_duration) throws AWTException, InterruptedException{
		childbenefitRider_illustration_flag = false;
		int basefaceamount_int = Integer.parseInt(BaseFaceAmount_Illustration);
		int premiumAmount_int = Integer.parseInt(PremiumAmount_Illustration);
		if(illustration.equalsIgnoreCase("Yes")) {
			extentTest.log(LogStatus.INFO, " - Illustration Policy Page - ");
			switchToFrame(frameIllustrationPolicy);
			ComboSelectValue(dropdownGender, gender, "Gender");
			if(SelectionIndAsYes(useUnisexRate, "UseUnisexRates")){
				ClickJSElement(cbUseUnisexRates, "UseUnisexRates");
			}
			ComboSelectValue(dropdownPrimaryInsuredClass, primaryInsuredClass, "Primary Insured Class");
			ClickJSElement(btnRating, "Rating");
			Thread.sleep(3000);
			switchToDefault();
			switchToFrame(frameRatingDetails);
			EnterText(txtPermanentFlatExtra, permanentFlatExtra, "PermanentFlatExtra");
			EnterText(txtTemporaryFlatExtra, temporaryFlatExtra, "TemporaryFlatExtra");
			EnterText(txtForYears, forYears, "ForYears");
			ClickJSElement(btnSave, "btnSave");
			Thread.sleep(3000);
			switchToDefault();
			switchToFrame(frameIllustrationPolicy);
			
			ComboSelectValue(dropdownPlan, Plan_Illustration, "Plan");
			ComboSelectValue(dropdownInputMethods, inputMethods, "InputMethods");
			if(inputMethods.equalsIgnoreCase("Base Face Amount")) {
				Thread.sleep(2000);
				txtIntialBaseFaceAmount.sendKeys(Keys.chord(Keys.CONTROL,"a"));
				txtIntialBaseFaceAmount.sendKeys(Keys.BACK_SPACE);
				txtIntialBaseFaceAmount.sendKeys(BaseFaceAmount_Illustration);
			}
			else {
				Thread.sleep(2000);
				txtPremiumAmount.sendKeys(Keys.chord(Keys.CONTROL,"a"));
				txtPremiumAmount.sendKeys(Keys.BACK_SPACE);
				txtPremiumAmount.sendKeys(PremiumAmount_Illustration);
			}
		Thread.sleep(2000);
		if(age<56) {
		ComboSelectValue(modeofpayment, Payment_mode, "mode of payment");
		if((inputMethods.equalsIgnoreCase("Base Face Amount") && basefaceamount_int<=5000000)||
				(inputMethods.equalsIgnoreCase("Premium Amount") && premiumAmount_int<1000000)) {
			if(WPD_illustration.equalsIgnoreCase("Yes")) {
				ClickElement(wpd_Rider_illustration, "Select WPD Rider button");
				wpd_Rider_duration.sendKeys(Keys.chord(Keys.CONTROL,"a"));
				wpd_Rider_duration.sendKeys(Keys.BACK_SPACE);
				wpd_Rider_duration.sendKeys(WPD_duration);	
				}}
			if(CBR_illustration.equalsIgnoreCase("Yes")) {
				ClickElement(CBR, "Select children benefit_Rider button");
				EnterText(CBRunit, CBR_Unit, "ForYears");
				EnterText(CBRduration, CBR_duration, "ForYears");
				childbenefitRider_illustration_flag = true;
				}}
		
		ClickElement(illustrations_btn, "Illustartion");
				
				Thread.sleep(30000);
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_ALT);
				r.keyPress(KeyEvent.VK_F4);
				r.keyRelease(KeyEvent.VK_F4);
				r.keyRelease(KeyEvent.VK_ALT);
				
				Thread.sleep(2000);
				switchToDefault();
				takeScreenshot("Illustration_PolicyPage");
				ClickJSElement(tabApplication, "Application Tab");
				
				Thread.sleep(15000);
				return new ProposedInsuredInfoAction();
		}
		else
		{
			return new ProposedInsuredInfoAction();
		}

	}

}
