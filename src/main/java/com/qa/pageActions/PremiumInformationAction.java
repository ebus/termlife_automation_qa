package com.qa.pageActions;

import com.qa.pages.PremiumInformationPage;
import com.relevantcodes.extentreports.LogStatus;

public class PremiumInformationAction extends PremiumInformationPage{
	
	public PremiumInformationAction() {
		super();
	}
	
	public USAPatriotActAction enterDataPremiumInformation(String illustration,String paymentMode, String otherMode) throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - Premium Information Page - ");
		switchToFrame(framePremiumInformation);
		if(illustration.equalsIgnoreCase("No")) {
		ComboSelectValue(dropdownPaymentMode, paymentMode, "Payment Mode");
		if(paymentMode.equalsIgnoreCase("Other")) {
			EnterText(txtOtherMode, otherMode, "Other Mode");
		}}
		else
		{
			ClickElement(add_premium_existing_policy, "add premium existing policy");
			EnterText(policyid, "POL_123", "Policy id");
		}
		ClickElement(rdoIsPolicyBeingFunded_No, "IsPolicyBeingFunded_No");
		takeScreenshot("PremiumInformationPage");
		ClickElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(5000);
		return new USAPatriotActAction();
	}
}
