package com.qa.pageActions;

import com.qa.pages.HealthQuestionsContPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.non_AUW_flag;

public class HealthQuestionsContPageAction extends HealthQuestionsContPage{
	public HealthQuestionsContPageAction() {
		super();
	}
	
	//Actions
	public HealthQuestionsChildrenPageAction enterHealthQueCont(String isCompletingHealthInformation) {
//		boolean flagHealthQuestion = (isTermConversion.equalsIgnoreCase("Yes") || flagAUW) && (!flagIdo_AUW);
//		if(flagAUW2_Qualified) {
//			return new FamilyHistoryPageAction();
//		}
//		if(isCompletingHealthInformation.equalsIgnoreCase("No") && flagAUW) {
//			return new FamilyHistoryPageAction();
//		}
		if(non_AUW_flag == false) {
			try {
				extentTest.log(LogStatus.INFO, " - Health Questions Page Cont - ");
				switchToFrame(frameHealthQuestionsCont);	
				ClickJSElement(rdoQuestionNo_1, "Question_1_No");
				ClickJSElement(rdoQuestionNo_2, "Question_2_No");
				ClickJSElement(rdoQuestionNo_3, "Question_3_No");
				ClickJSElement(rdoQuestionNo_4, "Question_4_No");
				ClickJSElement(rdoQuestionNo_5, "Question_5_No");
				ClickJSElement(rdoQuestionNo_6, "Question_6_No");
				ClickJSElement(rdoQuestionNo_7, "Question_7_No");	
				ClickJSElement(rdoQuestionNo_8, "Question_8_No");
				takeScreenshot("HealthQuestionsPageCont");
				ClickJSElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(5000);
				return new HealthQuestionsChildrenPageAction();
			}
			catch(Exception e) {
				e.printStackTrace();
				return new HealthQuestionsChildrenPageAction();
			}
//		}
//		else {
//			return new FamilyHistoryPageAction();
//		}
	}
		else
			return new HealthQuestionsChildrenPageAction();

}}
