package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.ExistingInsurancePage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.HIVConsentAction.flag;

public class ExistingInsuranceAction extends ExistingInsurancePage{
	
	public ExistingInsuranceAction() {
		super();
	}
	
	public PremiumInformationAction enterDataExistingInsurance(String state,String isExistingInsurance, String isPolicyReplacing, String is1035Exchange) throws InterruptedException, AWTException{
		String[] stateBucket = {"Georgia", "Illinois", "Minnesota", "Mississippi",  "North Dakota", "Tennessee","Wyoming"};
		boolean stateflag = false;
		for(String s : stateBucket) {
			if((state.equalsIgnoreCase(s))) {
				stateflag = false;
				break;
			}else {
				stateflag = true;
			}
		}		
		extentTest.log(LogStatus.INFO, " - Existing Insurance Page - ");
		switchToFrame(frameExistingInsurance);
		
		if(SelectionIndAsYes(isExistingInsurance, "Existing Insurance")) {
			ClickJSElement(rdoExistingInsYes, "Existing Insurance Yes");
			if(SelectionIndAsYes(isPolicyReplacing, "Policy Replacing")) {
				ClickJSElement(rdoPolicyReplacing_Yes, "PolicyReplacing Yes");
				
				if(flag == true || stateflag == true)
					{
					ClickJSElement(rdousingfundsfromyourexistingpolicies_No, "usingfundsfromyourexistingpolicies_No");
					ClickJSElement(rdoterminatingexistingPolicy_No, "terminatingexistingPolicy_No");
					EnterText(Existingpolicydetail, "Existingpolicydetail", "Existing policy detail");
					}
				else
				{
					ClickJSElement(rdoPolicySummaryStatement_No, "PolicySummaryStatement_No");

				}

			}
			else {
				ClickJSElement(rdoPolicyReplacing_No, "PolicyReplacing_No");
			}
			ClickJSElement(btnClickAdd, "Click Add");
			switchToDefault();
			Thread.sleep(5000);
			switchToFrame(frameExistingInsuranceDetails);
			ComboSelectValue(dropdownInsuringCompanyName, "AUL", "InsuringCompanyName");
			ComboSelectValue(dropdownTypeInsurance, "Annuity", "TypeInsurance");
			EnterText(txtPolicyNumber, "123456", "Policy Number");
			EnterText(txtAmountBenefit, "2345", "Amount benefit");
			EnterText(txtYearIssued, "2010", "Year Issued");
				
			if(SelectionIndAsYes(isPolicyReplacing, "Policy Replacing")) {				
				ClickJSElement(rdoBeingReplaced_Yes, "PolicyReplacing Yes");
				if(flag == true || stateflag == true) {
					ClickJSElement(Usedasfinancing, "Used as a source of financing");
				}
				else if(state.equalsIgnoreCase("Wyoming")) {
					EnterText(homeofficelocation, "123456", "home office location");
				}
				if(SelectionIndAsYes(is1035Exchange, "1035 Exchange")) {
					ClickJSElement(rdo1035Exchange_Yes, "1035Exchange_Yes");
//					ClickElement(rdoPolicyFinancing, "Policy_Financing");
					Thread.sleep(2000);
////					ClickElement(rdo1035InitPrem_Yes, "1035InitPrem_Yes");
					ClickElement(cb1stCheckbox, "1stCheckbox");
					ClickElement(rdoPolicyRequired, "PolicyRequired Radio Button");
				}
				else {
					ClickElement(rdo1035Exchange_No, "1035Exchange_No");
//					ClickElement(rdoPolicyFinancing, "Policy_Financing");
				}
			}
			else {
				ClickElement(rdoBeingReplaced_No, "PolicyReplacing_No");
			}
			Thread.sleep(3000);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(5000);
			switchToDefault();
			Thread.sleep(5000);
			switchToFrame(frameExistingInsurance);
		}
		else {
			ClickElement(rdoQuestionNo_1, "Question_1_No");
		}
		
		
		ClickJSElement(rdoQuestionNo_3, "Question_3_No");
		ClickJSElement(rdoQuestionNo_4, "Question_4_No");
//		if(flagJuvenile) {
//			EnterText(txtTotalAmountGuardian1, "1", "TotalAmountGuardian1");
//			EnterText(txtTotalAmountGuardian2, "1", "TotalAmountGuardian2");
//			EnterText(txtAgeAmount, "11", "AgeAmount");
//			EnterText(txtProvideDetails, "test", "ProvideDetails");
//		}
		if(flag == true || stateflag == true) {
			ClickJSElement(Noticereadaloud_no, "Question_3_No");
		}
		takeScreenshot("ExistingInsurancePage");
		ClickJSElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(5000);
		return new PremiumInformationAction();
	}
}
