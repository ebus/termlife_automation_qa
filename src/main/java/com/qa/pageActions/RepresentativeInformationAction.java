package com.qa.pageActions;

import java.awt.AWTException;

import org.openqa.selenium.WebElement;

import com.qa.pages.RepresentativeInformationPage;
import com.relevantcodes.extentreports.LogStatus;

public class RepresentativeInformationAction extends RepresentativeInformationPage{
	public RepresentativeInformationAction() {
		super();
	}
	
	//Actions 
	public ApplyESignatureSubmitAction enterDataRepresentativeInfo(String state,String isAdditionalRepresentatives, String AddRepresentativesCount, String representativesName1,
			String sharePercentage1,String license_Identification_No1, String representativesCode1, String representativesName2, String sharePercentage2, String license_Identification_No2,String representativesCode2,
			String representativesName3, String sharePercentage3, String license_Identification_No3, String representativesCode3, String representativesName4, String sharePercentage4, 
			String license_Identification_No4, String representativesCode4) throws InterruptedException, AWTException {
		extentTest.log(LogStatus.INFO, " - Representative Information Page - ");
		switchToFrame(frameRepresentativeInformation);
		ComboSelectValue(dropdownRepresentativeCode, "11253", "Representative Code");
		
		if(isEnabledElement(dropdownLicenseNumber)) {
			SelectByIndex(dropdownLicenseNumber, 1, "LicenseNumber");
		}
		
		EnterText(txtName, "TestName", "Name");
		EnterText(txtPhoneNumber, "3110012316", "Phone Number");
		
		if(isAdditionalRepresentatives.equalsIgnoreCase("Yes")) {
			ClickElement(rdoAdditionalRepresentatives_Yes, "AdditionalRepresentatives_Yes");
			
			switch(AddRepresentativesCount)
			{
				case "1":
				{ if(state.equalsIgnoreCase("Florida"))
					{
					additionalRepresentativeInfo_specialstate(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
							representativesCode1, txtRepresentativesCode,license_Identification_No,license_Identification_No1);

					break;
					}
				else {
					additionalRepresentativeInfo(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
							representativesCode1, txtRepresentativesCode);
					break;
					}
				}
				case "2":
				{
					if(state.equalsIgnoreCase("Florida"))
					{
						additionalRepresentativeInfo_specialstate(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
								representativesCode1, txtRepresentativesCode,license_Identification_No,license_Identification_No1);
						additionalRepresentativeInfo_specialstate(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
								representativesCode2, txtRepresentativesCode,license_Identification_No,license_Identification_No2);
						break;
					}
					else {
						additionalRepresentativeInfo(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
							representativesCode1, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
							representativesCode2, txtRepresentativesCode);
						break;
					}
				}
				case "3":
				{
					if(state.equalsIgnoreCase("Florida"))
					{
						additionalRepresentativeInfo_specialstate(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
							representativesCode1, txtRepresentativesCode,license_Identification_No,license_Identification_No1);
						additionalRepresentativeInfo_specialstate(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
							representativesCode2, txtRepresentativesCode,license_Identification_No,license_Identification_No2);
						additionalRepresentativeInfo_specialstate(representativesName3, txtRepresentativesName, sharePercentage3, txtRepresentativesPercentage, 
							representativesCode3, txtRepresentativesCode,license_Identification_No,license_Identification_No3);
						break;
					}
					else {
						additionalRepresentativeInfo(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
								representativesCode1, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
								representativesCode2, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName3, txtRepresentativesName, sharePercentage3, txtRepresentativesPercentage, 
								representativesCode3, txtRepresentativesCode);
						break;
						
					}
				}
				case "4":
				{
					if(state.equalsIgnoreCase("Florida"))
					{
						additionalRepresentativeInfo_specialstate(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
								representativesCode1, txtRepresentativesCode,license_Identification_No,license_Identification_No1);
						additionalRepresentativeInfo_specialstate(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
								representativesCode2, txtRepresentativesCode,license_Identification_No,license_Identification_No2);
						additionalRepresentativeInfo_specialstate(representativesName3, txtRepresentativesName, sharePercentage3, txtRepresentativesPercentage, 
								representativesCode3, txtRepresentativesCode,license_Identification_No,license_Identification_No3);
						additionalRepresentativeInfo_specialstate(representativesName4, txtRepresentativesName, sharePercentage4, txtRepresentativesPercentage, 
								representativesCode4, txtRepresentativesCode,license_Identification_No,license_Identification_No4);
						break;
					}
					else
					{
						additionalRepresentativeInfo(representativesName1, txtRepresentativesName, sharePercentage1, txtRepresentativesPercentage, 
								representativesCode1, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName2, txtRepresentativesName, sharePercentage2, txtRepresentativesPercentage, 
								representativesCode2, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName3, txtRepresentativesName, sharePercentage3, txtRepresentativesPercentage, 
								representativesCode3, txtRepresentativesCode);
						additionalRepresentativeInfo(representativesName4, txtRepresentativesName, sharePercentage4, txtRepresentativesPercentage, 
								representativesCode4, txtRepresentativesCode);
						break;
					}
			}
			
			}
			
			
		}else {
			ClickElement(rdoAdditionalRepresentatives_No, "AdditionalRepresentatives_No");
		}
		
		ClickElement(rdoQuestion2_No, "Question2_No");
		ClickElement(rdoQuestion3_Yes, "Question3_Yes");
		ClickElement(rdoQuestion4_Yes, "Question4_Yes");
		ClickElement(cbPhotoID, "PhotoID");	
		ClickElement(rdoShdAppEvaluated_No, "ShdAppEvaluated_No");	
		takeScreenshot("RepresentativeInformationPage");
		ClickElement(btnNext, "NextButton");	
		switchToDefault();
		Thread.sleep(5000);
		return new ApplyESignatureSubmitAction();
	}
	
	public void additionalRepresentativeInfo(String representativesName, WebElement txtRepresentativesName, String sharePercentage, 
			WebElement txtRepresentativesPercentage,  String representativesCode, WebElement txtRepresentativesCode) throws InterruptedException, AWTException {
		ClickElement(btnClickHere, "btnClickHere");
		Thread.sleep(5000);
		switchToDefault();
		switchToFrame(frameAddRepresentative);
		EnterText(txtRepresentativesName, representativesName, "RepresentativesName");
		EnterText(txtRepresentativesPercentage, sharePercentage, "RepresentativesPercentage");
		EnterText(txtRepresentativesCode, representativesCode, "RepresentativesCode");
		takeScreenshot("AddRepresentativeInformationPage");
		ClickJSElement(btnAgentSave, "btnAgentSave");
//		Robot r = new Robot();
//		r.keyPress(KeyEvent.VK_TAB);
//		r.keyRelease(KeyEvent.VK_TAB);
//		r.keyPress(KeyEvent.VK_ENTER);
//		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(8000);
		switchToDefault();
		switchToFrame(frameRepresentativeInformation);
	}
	public void additionalRepresentativeInfo_specialstate(String representativesName, WebElement txtRepresentativesName, String sharePercentage, 
			WebElement txtRepresentativesPercentage,  String representativesCode, WebElement txtRepresentativesCode, WebElement license_Identification_No, String licenseIdentificationNo) throws InterruptedException, AWTException {
		ClickElement(btnClickHere, "btnClickHere");
		Thread.sleep(5000);
		switchToDefault();
		switchToFrame(frameAddRepresentative);
		EnterText(txtRepresentativesName, representativesName, "RepresentativesName");
		EnterText(txtRepresentativesPercentage, sharePercentage, "RepresentativesPercentage");
		EnterText(license_Identification_No, licenseIdentificationNo, "license Identification No");
		EnterText(txtRepresentativesCode, representativesCode, "RepresentativesCode");
		takeScreenshot("AddRepresentativeInformationPage");
		ClickJSElement(btnAgentSave, "btnAgentSave");
//		Robot r = new Robot();
//		r.keyPress(KeyEvent.VK_TAB);
//		r.keyRelease(KeyEvent.VK_TAB);
//		r.keyPress(KeyEvent.VK_ENTER);
//		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(8000);
		switchToDefault();
		switchToFrame(frameRepresentativeInformation);
	}
}
