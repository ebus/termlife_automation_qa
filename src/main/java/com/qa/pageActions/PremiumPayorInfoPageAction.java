package com.qa.pageActions;

import com.qa.pages.PremiumPayorInfoPage;
import com.relevantcodes.extentreports.LogStatus;



public class PremiumPayorInfoPageAction extends PremiumPayorInfoPage{
	
	public PremiumPayorInfoPageAction() {
		super();
	}
	
	public BeneficiaryInfoPageAction enterPremiumPayorInfo(String whoWillBePayor, String payorRelationship,
			String payorEntityName, String payorFirstName, String payorLastName, String payorStreet, 
			String payorCity, String payorState, String payorZIP, String payorCountry, String payorYrsAtAdd, 
			String payorSSN, String payorDOB, String payorBirthState) {
		if(whoWillBePayor.equalsIgnoreCase("Other")) {
			try {
				extentTest.log(LogStatus.INFO, " - Premium Payor Information Page - ");
				switchToFrame(framePremiumPayorInfo);
				ComboSelectValue(dropdownRelationship, payorRelationship, "Relationship");
				
				if(payorRelationship.equalsIgnoreCase("Charitable Organization") || payorRelationship.equalsIgnoreCase("Employer")
							|| payorRelationship.equalsIgnoreCase("Trust")){
					EnterText(txtEntityName, payorFirstName, "EntityName");
					EnterText(txtEntityFirstName, payorFirstName, "First Name");
					EnterText(txtEntityLastName, payorLastName, "Last Name");
				}else if(payorRelationship.equalsIgnoreCase("Other")){
					EnterText(txtOtherProvideDetails, "Others", "Other Details");
					EnterText(txtFirstName, payorFirstName, "First Name");
					EnterText(txtLastName, payorLastName, "Last Name");
				}else {
					EnterText(txtFirstName, payorFirstName, "First Name");
					EnterText(txtLastName, payorLastName, "Last Name");
				}
				
				
				EnterText(txtStreetAddress, payorStreet, "Street Address");
				EnterText(txtCity, payorCity, "City");
				ComboSelectValue(dropdownState, payorState, "State");
				EnterText(txtZip, payorZIP, "Zip");
				EnterText(txtCounty, payorCountry, "Country");
				EnterText(txtYearsThisAdd, payorYrsAtAdd, "Years at This Address");
				EnterText(txtSSN, payorSSN, "SSN");
				enterDOB_Payor(payorDOB);
				ComboSelectValue(dropdownBirthState, payorBirthState, "Birth State");
				ClickElement(rdoGender_Male, "Gender_Male");
				ClickElement(rdoMarried, "Married");
				ComboSelectValue(dropdownEmpStatus, "Retired", "Employment Status");
				takeScreenshot("PremiumPayorInfoPage");
				ClickElement(btnNext, "Next");				
				switchToDefault();
				Thread.sleep(5000);
				return new BeneficiaryInfoPageAction();
			}
			catch(Exception e) {
				e.printStackTrace();
				return new BeneficiaryInfoPageAction();
			}
		}
		else {
			return new BeneficiaryInfoPageAction();
		}
	}
	public void enterDOB_Payor(String dob)
	{
		enterDOB(dob, txtMonth, txtDay, txtYear);
	}

}
