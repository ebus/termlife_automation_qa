package com.qa.pageActions;

import com.qa.pages.ChildrenBenefitRiderPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.TermLifeInsuranceCoverageRidersAction.childbenefitRiderflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.childbenefitRider_illustration_flag;


public class ChildrenBenefitRiderAction extends ChildrenBenefitRiderPage {
	
	public ChildrenBenefitRiderAction() {
		super();
	}	
	
	public AcceleratedQualifiersPageAction detailsOfChildrenBenefitRider(String child_firstname,String child_lastname,String child_relationship,String child_gender,String child_height_ft,String child_height_in,String child_weight,String child_dob,String child_all_listed) throws InterruptedException {
		if(childbenefitRiderflag == true || childbenefitRider_illustration_flag == true )
		{
			extentTest.log(LogStatus.INFO, " - Children Benefit Rider Page - ");
			switchToFrame(frameChildrenBenefitRider);
			ClickElement(btnAddTheChildrenDetails, "Add ChildernDetails Button");
			switchToDefault();
			switchToFrame(frameChildrenDetailsPage);
			Thread.sleep(3000);
			EnterText(firstName, child_firstname, "First Name");
			EnterText(lastName, child_lastname, "Last Name");		
			ComboSelectValue(relationship, child_relationship,"Relationship");
			selectGender(child_gender, gender_male, gender_female);
			ComboSelectValue(height_ft, child_height_ft,"Height in ft");
			ComboSelectValue(height_in, child_height_in,"Height in inc");
			EnterText(weight, child_weight, "Weight");
			enterDOB(child_dob,txtbirthMonth,txtbirthDay,txtbirthYear);
			ClickJSElement(save_btn, "SaveButton");
			switchToDefault();
			switchToFrame(frameChildrenBenefitRider);
			ClickJSElement(childlisted_btn_yes, "Click on child listed yes button");
			Thread.sleep(5000);
			takeScreenshot("ChildrenBenefitRiderPage");
			ClickJSElement(next_btn, "NextButton");
			switchToDefault();			
			
			return new AcceleratedQualifiersPageAction();
		}
			
		else 
			return new AcceleratedQualifiersPageAction();
		
	}
	
}
