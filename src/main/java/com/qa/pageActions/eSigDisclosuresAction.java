package com.qa.pageActions;

import com.qa.pages.eSigDisclosuresPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ProposedInsuredInfoAction.flagJuvenile;

public class eSigDisclosuresAction extends eSigDisclosuresPage{
	
	public eSigDisclosuresAction() {
		super();
	}
	
	//Actions
	public eSignatureConsentAction enterDataESigDisclosures(String willTheProposedInsuredBeOwner, String isMultipleOwner, 
			 String whoWillBePayor, String indLegalGuardianPolicyOwner) throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - eSigDisclosures Page - ");
		switchToFrame(frameESigDisclosures);
		if(!flagJuvenile) {
			ClickElement(rdoProInsYes, "Proposed Insured Yes radio button");
			ComboSelectValue(dropdownIdentification, "Drivers License", "Proof of Identification");
		}
		
		if(willTheProposedInsuredBeOwner.equalsIgnoreCase("No")) {
			ClickElement(rdoProInsYes_Owner, "Proposed Insured Yes radio button for Owner");
			ComboSelectValue(dropdownIdentification_Owner, "Drivers License", "Proof of Identification for Owner");
		}
		
//		if(isSurvivorPurchaseOptionRider.equalsIgnoreCase("Yes")) {
//			ClickElement(rdoProInsYes_Survivor, "Proposed Insured Yes radio button for Owner");
//			ComboSelectValue(dropdownIdentification_Survivor, "Drivers License", "Proof of Identification for Owner");
//		}
		
		takeScreenshot("eSigDisclosuresPage");
		ClickElement(btnNext, "Next Button");
		Thread.sleep(5000);
		
		boolean flag_MO = willTheProposedInsuredBeOwner.equalsIgnoreCase("No") && isMultipleOwner.equalsIgnoreCase("Yes");
		boolean flag_Payor = whoWillBePayor.equalsIgnoreCase("Other");
		
		if(flagJuvenile) {
			if(indLegalGuardianPolicyOwner.equalsIgnoreCase("No")) {
				ClickElement(rdoLG_Yes, "Proposed Insured Yes radio button");
				ComboSelectValue(dropdownIdentification_LG, "Drivers License", "Proof of Identification");
			}
		}
		
		if(flag_MO) {
			ClickElement(rdoProInsYes_MO, "Proposed Insured Yes radio button for Multiple Owner");
			ComboSelectValue(dropdownIdentification_MO, "Drivers License", "Proof of Identification for Owner");
			takeScreenshot("eSigDisclosuresPageCo_Owner");		
		}
		
		if(flag_Payor) {
			ClickElement(rdoProInsYes_Payor, "Proposed Insured Yes radio button for Multiple Payor");
			ComboSelectValue(dropdownIdentification_Payor, "Drivers License", "Proof of Identification for Payor");
			takeScreenshot("eSigDisclosuresPageCo_Payor");
		}
		
		if(flag_MO || flag_Payor) {
			ClickElement(btnNext2, "Next Button");
		}
		
		
		switchToDefault();
		Thread.sleep(5000);
		return new eSignatureConsentAction();
	}

}
