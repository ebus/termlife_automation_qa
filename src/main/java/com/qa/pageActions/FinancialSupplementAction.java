package com.qa.pageActions;

import com.qa.pages.FinancialSupplementPage;
import com.relevantcodes.extentreports.LogStatus;


public class FinancialSupplementAction extends FinancialSupplementPage{
	
	public FinancialSupplementAction() {
		super();
	}
	public static boolean purpose_of_Insurance_business_flag;
	public FinancialSupplementContAction enterDataFinancialSupplement(String illustration,String BaseFaceAmount_Illustration,String faceamount,String Salary_currentyear,String Salary_lastyr,String Salary_2yrsago,String Pension_currentyear,String Pension_lastyr,String Pension_2yrsago,String Bonus_currentyear,
			String Bonus_lastyr,String Bonus_2yrsago,String Earnings_currentyear,String Earnings_lastyr,String Earnings_2yrsago,String Deduction_currentyear,String Deductyion_lastyr,
			String Deduction_2yrsago,String Dividends,String Interest,String NetCapGain,String Rentalincome,String other_income,String Cash_savings_net,String Interest_Business,
			String Personal_Property,String Real_Estate,String Other_Real_Estate,String Other_networth,String Purpose_Insurance) throws InterruptedException {
		int faceamount_int = Integer.parseInt(faceamount);
		int faceamount_illus_int = Integer.parseInt(BaseFaceAmount_Illustration);
		if(illustration.equalsIgnoreCase("No")&&faceamount_int > 5000000 ||illustration.equalsIgnoreCase("Yes")&&faceamount_illus_int > 5000000) {
			purpose_of_Insurance_business_flag = false;
			extentTest.log(LogStatus.INFO, " -  Financial Supplement-Primary Insured - ");
			switchToFrame(frameFinancialSupplement);
			EnterText(salary_currentyear, Salary_currentyear, "current year salary");
			EnterText(salary_lastyear, Salary_lastyr, "last year salary");
			EnterText(salary_2yearsago, Salary_2yrsago, "2years ago salary");
			EnterText(pension_currentyear, Pension_currentyear, "current year pension");
			EnterText(pension_lastyear, Pension_lastyr, "last year pension");
			EnterText(pension_2yearsago, Pension_2yrsago, "2years ago pension");
			EnterText(bonus_currentyear, Bonus_currentyear, "current year bonus");
			EnterText(bonus_lastyear, Bonus_lastyr, "Last Year bonus");
			EnterText(bonus_2yearsago, Bonus_2yrsago, "2years ago bonus");
			EnterText(otheroccupations_currentyear, Earnings_currentyear, "current year income from otheroccupations");
			EnterText(otheroccupations_lastyear, Earnings_lastyr, "last year income from otheroccupations");
			EnterText(otheroccupations_2yearsago, Earnings_2yrsago, "2years ago income from other occupations");
			EnterText(deductibleExpenses_currentyear, Deduction_currentyear, "current year deductible Expenses");
			EnterText(deductibleExpenses_lastyear, Deductyion_lastyr, "last year income deductible Expenses");
			EnterText(deductibleExpenses_2yearsago, Deduction_2yrsago, "2years ago deductible Expenses");
			
			EnterText(dividends, Dividends, "dividends value");
			EnterText(interest, Interest, "interest value");
			EnterText(net_Realized_Capital_Gains, NetCapGain, "net_Realized_Capital_Gains");
			EnterText(rental_income, Rentalincome, "rental_income");
			EnterText(other, other_income, "other value");
			
			EnterText(Cash_savings, Cash_savings_net, "value of Cash_savings");
			EnterText(business_interest, Interest_Business, "value of business_interest");
			EnterText(personal_property, Personal_Property, "personal_property");
			EnterText(realestateresidence, Real_Estate, "realestateresidence");
			EnterText(other_Real_Estate, Other_Real_Estate, "other_Real_Estate");
			EnterText(other_networth_details, Other_networth, "other networth details");
	
			if(Purpose_Insurance.equalsIgnoreCase("Business")) {
				ClickJSElement(purpose_of_Insurance_business, "Check box purpose of Insurance business");
				purpose_of_Insurance_business_flag = true;
			}
			else
				ClickJSElement(purpose_of_Insurance_personal, "Check box purpose of Insurance personal");
			takeScreenshot("FinancialSupplementPage");
			ClickElement(btnNext, "Next button");	
			switchToDefault();
			Thread.sleep(5000);
			return new FinancialSupplementContAction();
		}
		else
			return new FinancialSupplementContAction();
	}
}
