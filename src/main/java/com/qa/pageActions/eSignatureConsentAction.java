package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.eSignatureConsentPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ProposedInsuredInfoAction.flagJuvenile;

public class eSignatureConsentAction extends eSignatureConsentPage{
	
	public eSignatureConsentAction() {
		super();
	}
	
	//Actions
	public eSignaturePartiesAction enterDataESignatureConsent(String willTheProposedInsuredBeOwner, 
			String isMultipleOwner, String whoWillBePayor,
			String indLegalGuardianPolicyOwner) throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver, 100); 
		extentTest.log(LogStatus.INFO, " - eSignatureConsent Page - ");
		switchToFrame(frameeSignatureConsent);
		scrollIntoView(btnReviewApplication, driver);
		ClickElement(btnReviewApplication, "Review Application button");
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		Thread.sleep(5000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		
		if(!flagJuvenile) {
			ClickElement(cbApplicants, "Applicants CheckBox");
		}else {
			if(indLegalGuardianPolicyOwner.equalsIgnoreCase("No")) {
				ClickElement(cbLG, "Legal Gurdian CheckBox");
			}
		}
		
		ClickElement(cbAgent, "Agent CheckBox");
		if(willTheProposedInsuredBeOwner.equalsIgnoreCase("No")) {
			ClickElement(cbOwner, "Owner Checkbox");
			if(isMultipleOwner.equalsIgnoreCase("Yes")) {
				ClickElement(cbMO, "Owner Checkbox");
			}
		}
		
//		if(isSurvivorPurchaseOptionRider.equalsIgnoreCase("Yes")) {
//			ClickElement(cbSurvivor, "Survivor Checkbox");
//		}
		if(whoWillBePayor.equalsIgnoreCase("Other")) {
			ClickElement(cbPayor, "Payor Checkbox");
		}
		
		
		takeScreenshot("eSignatureConsentPage");
		ClickElement(btnNext, "NextButton");
		switchToDefault();
		Thread.sleep(5000);
		return new eSignaturePartiesAction();
	}
}
