package com.qa.pageActions;

import static com.qa.pageActions.ProposedInsuredInfoAction.insuredAge;
//import static com.qa.pageActions.Illustration_PolicyPageAction.childbenefitRiderflag_illustration;

import com.qa.pages.TermLifeInsuranceCoverageRidersPage;
import com.relevantcodes.extentreports.LogStatus;

public class TermLifeInsuranceCoverageRidersAction extends TermLifeInsuranceCoverageRidersPage {
	public TermLifeInsuranceCoverageRidersAction() {
		super();
	}
	public static boolean childbenefitRiderflag;
	public static boolean cashwithappflag;
	public static boolean flagAUW;
	public static boolean flagAUW2;
	public static boolean Completeformflag;
	
	public ChildrenBenefitRiderAction enterDataTermLifeInsuranceCoverageRiders(String illustration,String BaseFaceAmount_Illustration,String faceamount, String Plan,String cashwithapp,String premiumamount,String wpdRider,String childrenbenefitRider,String Unit,String Other,String Othervalue,String Completeformvalue,String concurrentcoverage,String amountexceed)throws InterruptedException {
		String waiverconvflag = "Yes";
		childbenefitRiderflag = false;
		Completeformflag = false;
		cashwithappflag = false;
		flagAUW = false;
		flagAUW2 = false;
        int faceamount_int = Integer.parseInt(faceamount);
        int faceamount_illustration_int = Integer.parseInt(BaseFaceAmount_Illustration);
		flagAUW = (insuredAge>=18 && insuredAge<=50);
		extentTest.log(LogStatus.INFO, " - Term Life Insurance Coverage Page - ");
		switchToFrame(TermLifeInsuranceCoverageRiders);
		if(illustration.equalsIgnoreCase("No")) {
			EnterText(faceAmount, faceamount, "Base Face Amount");
			ComboSelectValue(term, Plan, "Plan");
		}
		if(cashwithapp.equalsIgnoreCase("Yes")&&faceamount_int<=2000000 ) {
			ClickJSElement(cashwithapp_yes, "Cash with app yes");
			EnterText(premiumAmount, premiumamount, "Premium Amount");
			cashwithappflag = true;	
		}
		else
			ClickJSElement(cashwithapp_no, "Cash with app no");
		if(illustration.equalsIgnoreCase("No")) {
			if(insuredAge <= 55) {
				if(wpdRider.equalsIgnoreCase("Yes") && faceamount_int <= 5000000) {
					ClickElement(wpd_Rider, "Select WPD Rider button");
					if(waiverconvflag == "Yes") {
						ClickJSElement(waiverconversion_yes, "Waiver conversion yes");
						}
					else	
						ClickJSElement(waiverconversion_no, "Waiver conversion no");
					}
				if(childrenbenefitRider.equalsIgnoreCase("Yes")) {
					ClickElement(children_benefit_Rider, "Select children benefit_Rider button");
					ComboSelectValue(unit, Unit, "Unit");
					childbenefitRiderflag = true;
					}
				
		}
			if(Other.equalsIgnoreCase("Yes")) {
				ClickElement(other, "Select other button");
				EnterText(other_value, Othervalue, "Other value");
				
				}
			}
		if(Completeformvalue.equalsIgnoreCase("Yes")) 
			ClickJSElement(Compltformflag_yes, "Complete the required Whole Life forms with this application");
		else
		{	
			ClickJSElement(Compltformflag_no, "Deselect Complete the required Whole Life forms with this application");
			Completeformflag = true;
		}
		if((flagAUW == true && faceamount_int<=1000000)||(flagAUW == true && faceamount_illustration_int<=1000000)) {
			if(concurrentcoverage.equalsIgnoreCase("Yes")) {
				ClickJSElement(concurrent_coverage_yes, "concurrent coverage yes");
				if(amountexceed.equalsIgnoreCase("Yes")) {
					ClickJSElement(amount_exceed_yes, "amount exceed yes");
				}
				else {
					ClickJSElement(amount_exceed_no, "amount exceed no");
					flagAUW2 = true;
				}
				
			}
			else
			{
				ClickJSElement(concurrent_coverage_no, "concurrent coverage no");
				flagAUW2 = true;
			}
		
		}
	
		takeScreenshot("TermLifeInsuranceCoverageRidersPage");
		ClickElement(btnNext, "Next button");
		Thread.sleep(5000);
		switchToDefault();
		Thread.sleep(3000);
		//
		
		return new ChildrenBenefitRiderAction();
}}
