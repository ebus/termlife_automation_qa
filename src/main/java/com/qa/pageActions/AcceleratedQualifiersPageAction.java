package com.qa.pageActions;

import com.qa.pages.AcceleratedQualifiersPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.TermLifeInsuranceCoverageRidersAction.flagAUW2;
import static com.qa.pageActions.CaseInformationPageAction.age;


public class AcceleratedQualifiersPageAction extends AcceleratedQualifiersPage{
	public AcceleratedQualifiersPageAction() {
		super();
	}	
	
	public static boolean AUWflag = false;
	public static boolean flagIdo_AUW = false;
	public static boolean flagAUW2_Qualified = false;
	public static boolean non_AUW_flag = false;
	//Actions
	public PersonalInformationAction enterAUWData(String illustration,String BaseFaceAmount_Illustration,String faceamount,String isAUW, String question_AUW1,String question_AUW2,String question_AUW3 ) throws InterruptedException {
		AUWflag = (age>=18 && age<=50) ;//&& faceAmount <= 1000000 && isTermConversion.equalsIgnoreCase("No");
		int faceamount_int =Integer.parseInt(faceamount);
		int faceamount_illustration_int =Integer.parseInt(BaseFaceAmount_Illustration);
		if(illustration.equalsIgnoreCase("No")) {
			if((flagAUW2 && AUWflag && faceamount_int <= 1000000)) {
			extentTest.log(LogStatus.INFO, " - Accelerated Qualifiers Page - ");
			switchToFrame(frameAcceleratedQualifiers);
			if(question_AUW1.equalsIgnoreCase("Yes")) {
			ClickElement(rdo_No_Q1_Yes, "rdo_No_Q1");
			EnterText(Q1_Details, "Medical History", "Accelerated Underwriting Details");
			}
			else { 
				ClickElement(rdo_No_Q1_No, "rdo_No_Q1");
				if(question_AUW2.equalsIgnoreCase("Yes")) {
					ClickElement(rdo_No_Q2_Yes, "rdo_No_Q2");
					}
				else
				{
					ClickElement(rdo_No_Q2_No, "rdo_No_Q2");
					if(question_AUW3.equalsIgnoreCase("Yes")) {
						ClickElement(rdo_No_Q3_Yes, "rdo_No_Q3");
					}
						else
						{
							ClickElement(rdo_No_Q3_No, "rdo_No_Q3");
							non_AUW_flag = true;
							ClickElement(cb_Authorization, "cb_Authorization");
							ClickElement(btnInitiateProcess, "InitiateProcess");
						}
				}
			}

			scrollIntoView(rdo_No_Q3_No, driver);
			takeScreenshot("AcceleratedQualifierPage");
			Thread.sleep(30000);
			try {
				if(labelAUWQualified.isDisplayed()) {
					flagAUW2_Qualified = true;				
				}
			}catch(Exception e) {
				
			}
//			Thread.sleep(90000);
			takeScreenshot("AcceleratedQualifierPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new PersonalInformationAction();	
		}
			else 
				return new PersonalInformationAction();
		}
	
		else
		{
			if((flagAUW2 && AUWflag && faceamount_illustration_int <= 1000000)) {
				extentTest.log(LogStatus.INFO, " - Accelerated Qualifiers Page - ");
				switchToFrame(frameAcceleratedQualifiers);
				if(question_AUW1.equalsIgnoreCase("Yes")) {
				ClickElement(rdo_No_Q1_Yes, "rdo_No_Q1");
				EnterText(Q1_Details, "Medical History", "Accelerated Underwriting Details");
				}
				else { 
					ClickElement(rdo_No_Q1_No, "rdo_No_Q1");
					if(question_AUW2.equalsIgnoreCase("Yes")) {
						ClickElement(rdo_No_Q2_Yes, "rdo_No_Q2");
						}
					else
					{
						ClickElement(rdo_No_Q2_No, "rdo_No_Q2");
						if(question_AUW3.equalsIgnoreCase("Yes")) {
							ClickElement(rdo_No_Q3_Yes, "rdo_No_Q3");
						}
							else
							{
								ClickElement(rdo_No_Q3_No, "rdo_No_Q3");
								non_AUW_flag = true;
								ClickElement(cb_Authorization, "cb_Authorization");
								ClickElement(btnInitiateProcess, "InitiateProcess");
							}
					}
				}

				scrollIntoView(rdo_No_Q3_No, driver);
				takeScreenshot("AcceleratedQualifierPage");
				Thread.sleep(30000);
				try {
					if(labelAUWQualified.isDisplayed()) {
						flagAUW2_Qualified = true;				
					}
				}catch(Exception e) {
					
				}
//				Thread.sleep(90000);
				takeScreenshot("AcceleratedQualifierPage");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(5000);
				return new PersonalInformationAction();	
			}
				else 
				return new PersonalInformationAction();
		}
}}
