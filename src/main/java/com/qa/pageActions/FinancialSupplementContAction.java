package com.qa.pageActions;

import com.qa.pages.FinancialSupplementContPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.FinancialSupplementAction.purpose_of_Insurance_business_flag;

public class FinancialSupplementContAction extends FinancialSupplementContPage{
	
	public FinancialSupplementContAction() {
		super();
	}

	public TemporaryInsuranceAgreementAction enterDataFinancialSupplementCont(String illustration,String BaseFaceAmount_Illustration,String faceamount) throws InterruptedException {
		int faceamount_int = Integer.parseInt(faceamount);
		int faceamount_illustration_int = Integer.parseInt(BaseFaceAmount_Illustration);
		String creditor_value = "Yes";
		if((illustration.equalsIgnoreCase("No") && faceamount_int > 5000000 && purpose_of_Insurance_business_flag == true)
				||(illustration.equalsIgnoreCase("Yes") && faceamount_illustration_int > 5000000 && purpose_of_Insurance_business_flag == true)) {
			extentTest.log(LogStatus.INFO, " -  Confidential Financial Supplement, Cont - ");
			switchToFrame(frameFinancialSupplementCont);
			ComboSelectValue(purpose_Business_Insurance, "Key Executive", "Business Insurance Purpose");
			if(creditor_value.equalsIgnoreCase("No"))
				ClickJSElement(creditor_No, "creditor No");
			else {
				ClickJSElement(creditor_Yes, "creditor Yes");
				ClickJSElement(insurance_requested_by_lender_yes, "insurance requested by lender yes");
				EnterText(lender_name, "Harry", "lender Name");
				EnterText(coverage_amount_required_Creditor, "800", "coverage amount required Creditor");
				EnterText(loan_amount, "5000", "loan amount");
				EnterText(loan_duration, "5Y", "loan duration");
				EnterText(loan_purpose, "Personal", "loan purpose");
				EnterText(other_purposes, "NA", "other purposes");
			}
			ClickJSElement(officers_Partners_being_insured_no, "officers or Partners being insured no");
			EnterText(current_year_to_date, "900", "last year salary");
			EnterText(current_year_to_date_thru, "800", "2years ago salary");
			EnterText(total_assets_currentyear, "1000", "total assets currentyear");
			EnterText(total_assets_prevyear, "900", "total assets previous year");
			EnterText(total_liabilities_currentyear, "100", "total liabilities currentyear");
			EnterText(total_liabilities_prevyear, "100", "total liabilities previous year");
			EnterText(GrosssalesRevenue_currentyear, "1000", "GrosssalesRevenue currentyear");
			EnterText(GrosssalesRevenue_prevyear, "900", "GrosssalesRevenue previous year");
			EnterText(netincome_currentyear, "100", "net income currentyear");
			EnterText(netincome_prevyear, "100", "net income previous year");
			
			takeScreenshot("FinancialSupplementContPage");
			ClickElement(btnNext, "Next button");	
			switchToDefault();
			Thread.sleep(5000);
			return new TemporaryInsuranceAgreementAction();
		}
		else
			return new TemporaryInsuranceAgreementAction();
	}
}
