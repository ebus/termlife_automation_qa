package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class Illustration_PolicyPage extends GenericFunction{
	//Initializing the Page Objects:
	public Illustration_PolicyPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameIllustrationPolicy;
	
	@FindBy(css="select#lb_20")
	public WebElement dropdownGender;
	
	@FindBy(css="#cb_68")
	public WebElement cbUseUnisexRates;
	
	@FindBy(css="select#lb_11")
	public WebElement dropdownPrimaryInsuredClass;
	
	@FindBy(css="#btn_9")
	public WebElement btnRating;
	
	@FindBy(id="modalIframe")
	public WebElement frameRatingDetails;
	
	@FindBy(css="input#flda_4")
	public WebElement txtPermanentFlatExtra;
	
	@FindBy(css="input#flda_6")
	public WebElement txtTemporaryFlatExtra;
	
	@FindBy(css="input#flda_8")
	public WebElement txtForYears;
	
	@FindBy(css="#btn_10")
	public WebElement btnSave;
	
	@FindBy(id ="lb_66")
	public WebElement dropdownPlan;
	
	@FindBy(css="select#lb_69")
	public WebElement dropdownInputMethods;
	
	@FindBy(css="input#flda_4")
	public WebElement txtIntialBaseFaceAmount;
	
	@FindBy(css="input#flda_30")
	public WebElement txtPremiumAmount;
	
	@FindBy(id = "lb_6")
	public WebElement modeofpayment;
	
	@FindBy(css="input#cb_74")
	public WebElement wpd_Rider_illustration;
	
	@FindBy(css="input#flda_86")
	public WebElement wpd_Rider_duration;
	
	@FindBy(id = "lb_77")
	public WebElement wpd_conversion_option;
	
	@FindBy(css="input#cb_81")
	public WebElement CBR;
	
	@FindBy(css="input#flda_89")
	public WebElement CBRunit;
	
	@FindBy(css="input#flda_82")
	public WebElement CBRduration;
	
	@FindBy(css="#lbl_57")
	public WebElement txtInputMethods;//
	
	@FindBy(css="#cb_74")
	public WebElement waiverofpremiumdisabilitybtn;
	
	@FindBy(css="#flda_86")
	public WebElement duration;
	
	@FindBy(css="#cb_81")
	public WebElement child_benefit_riderbtn;
	
	@FindBy(css="#flda_89")
	public WebElement units;
	
	@FindBy(css="#flda_82")
	public WebElement duration_child_rider;
	
	@FindBy(css="#cb_85")
	public WebElement input_summary_btn;
	
	@FindBy(css="#btnIllustrations")
	public WebElement illustrations_btn;
	
	@FindBy(css="#spanApplication")
	public WebElement tabApplication;
	
	
	
	
	
	@FindBy(css="select#lb_7")
	public WebElement dropdownDividends;
	
	@FindBy(css="#btn_5")
	public WebElement btnSaveDividendOption;
	
	@FindBy(id="modalIframe")
	public WebElement frameDividendOptionChange;
	
	@FindBy(css="#cb_49")
	public WebElement cbDividendOptionChange;
	
	@FindBy(css="#grdx8_rc_0_0")
	public WebElement txtFromAgeDividendOptionChange;
	
	@FindBy(css="input#flda_52")
	public WebElement txtPercentageDividendScale;
	
	@FindBy(css="input#cb_13")
	public WebElement cbScheduleLoansWithdrawals;
	
	@FindBy(css="select#lb_59")
	public WebElement dropdownWithdrawalOptions;
	
	@FindBy(css="select#lb_21")
	public WebElement dropdownLoanInterest;
	
	@FindBy(css="select#lb_84")
	public WebElement dropdownDistributionSolves;

	@FindBy(css="select#lb_28")
	public WebElement dropdownPaymentMode;

	@FindBy(css="#rdo_155_1")
	public WebElement rdoIsTermConversion_Yes;
	
	@FindBy(css="#rdo_155_2")
	public WebElement rdoIsTermConversion_No;
	
	@FindBy(css="#btn_156")
	public WebElement btnNext;
}
