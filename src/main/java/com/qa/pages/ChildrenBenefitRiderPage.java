package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ChildrenBenefitRiderPage extends GenericFunction {
	
	//Page Factory - OR:
		@FindBy(id="CossScreenFrame")
		public WebElement frameChildrenBenefitRider;
		
		@FindBy(id="grdx4_addRowButton")
		public WebElement btnAddTheChildrenDetails;
		
		@FindBy(id="modalIframe")
		public WebElement frameChildrenDetailsPage;
		
		@FindBy(id="flda_13")
		public WebElement firstName;
		
		@FindBy(id="flda_30")
		public WebElement lastName;
		
//		@FindBy (xpath = "//span/input[@class='jq-dte-month jq-dte-is-required hint']")
//		public WebElement txtbirthMonth;
		
		@FindBy (xpath = "//span/input[contains(@class,'jq-dte-month')]")
		public WebElement txtbirthMonth;
		
//		@FindBy (xpath = "//span/input[@class='jq-dte-day jq-dte-is-required hint']")
//		public WebElement txtbirthDay;
		
		@FindBy (xpath = "//span/input[contains(@class,'jq-dte-day')]")
		public WebElement txtbirthDay;
		
//		@FindBy (xpath = "//span/input[@class='jq-dte-year jq-dte-is-required hint']")
//		public WebElement txtbirthYear;
		
		@FindBy (xpath = "//span/input[contains(@class,'jq-dte-year')]")
		public WebElement txtbirthYear;
		
		@FindBy(id="lb_27")
		public WebElement relationship;
		
		@FindBy(id="rdo_2_1")
		public WebElement gender_male;
		
		@FindBy(id="rdo_2_2")
		public WebElement gender_female;
		
		@FindBy(id="lb_18")
		public WebElement height_ft;
		
		@FindBy(id="lb_17")
		public WebElement height_in;
		
		@FindBy(id="flda_4")
		public WebElement weight;
		
		@FindBy(id="btn_12")
		public WebElement save_btn;	
		
		@FindBy(id="rdo_11_1")
		public WebElement childlisted_btn_yes;	
		
		@FindBy(id="rdo_11_2")
		public WebElement childlisted_btn_no;	
		
		@FindBy(id="btn_6")
		public WebElement next_btn;	
		
		
	//Initializing the Page Objects:
			public ChildrenBenefitRiderPage() {
				super();
				PageFactory.initElements(driver, this);
			}

}
