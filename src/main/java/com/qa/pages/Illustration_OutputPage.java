package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class Illustration_OutputPage extends GenericFunction{
	//Initializing the Page Objects:
	public Illustration_OutputPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameIllustrationOutput;
	
	@FindBy(css="#btnIllustrations")
	public WebElement btnIllustration;
	
}
