package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class FinancialSupplementPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameFinancialSupplement;
	
	@FindBy(id="flda_4")
	public WebElement salary_currentyear;
	
	@FindBy(id="flda_29")
	public WebElement salary_lastyear;
	
	@FindBy(id="flda_55")
	public WebElement salary_2yearsago;
	
	@FindBy(id="flda_26")
	public WebElement pension_currentyear;
	
	@FindBy(id="flda_30")
	public WebElement pension_lastyear;
	
	@FindBy(id="flda_54")
	public WebElement pension_2yearsago;
	
	@FindBy(id="flda_27")
	public WebElement bonus_currentyear;
	
	@FindBy(id="flda_31")
	public WebElement bonus_lastyear;
	
	@FindBy(id="flda_53")
	public WebElement bonus_2yearsago;
	
	@FindBy(id="flda_28")
	public WebElement otheroccupations_currentyear;
	
	@FindBy(id="flda_32")
	public WebElement otheroccupations_lastyear;
	
	@FindBy(id="flda_52")
	public WebElement otheroccupations_2yearsago;
	
	@FindBy(id="flda_37")
	public WebElement deductibleExpenses_currentyear;
	
	@FindBy(id="flda_36")
	public WebElement deductibleExpenses_lastyear;
	
	@FindBy(id="flda_50")
	public WebElement deductibleExpenses_2yearsago;
		
	@FindBy(id="flda_41")
	public WebElement dividends;	
	
	@FindBy(id="flda_35")
	public WebElement interest;
	
	@FindBy(id="flda_57")
	public WebElement net_Realized_Capital_Gains;
	
	@FindBy(id="flda_40")
	public WebElement rental_income;
	
	@FindBy (id = "flda_70")
	public WebElement other;
		
	@FindBy(id="flda_66")
	public WebElement Cash_savings;
	
	@FindBy(id="flda_65")
	public WebElement business_interest;
	
	@FindBy(id="flda_58")
	public WebElement personal_property;
	
	@FindBy(id="flda_39")
	public WebElement realestateresidence;
	
	@FindBy(id="flda_64")
	public WebElement other_Real_Estate;
	
	@FindBy(id="flda_73")
	public WebElement other_networth_details;
	
	@FindBy(id="rdo_74_1")
	public WebElement purpose_of_Insurance_personal;
	
	@FindBy(id="rdo_74_2")
	public WebElement purpose_of_Insurance_business;
	
	@FindBy(id="btn_7")
	public WebElement btnNext;
	
	
	//Initializing the Page Objects:
	public FinancialSupplementPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
