package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ExistingInsurancePage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameExistingInsurance;
	
	@FindBy(id="modalIframe")
	public WebElement frameExistingInsuranceDetails;
	
	@FindBy(id="rdo_19_2")
	public WebElement rdoQuestionNo_1;
	
	@FindBy(id="rdo_19_1")
	public WebElement rdoExistingInsYes;
	
	@FindBy(id="rdo_28_1")
	public WebElement rdoPolicyReplacing_Yes;
	
	
	@FindBy(id="rdo_20_2")
	public WebElement rdoPolicySummaryStatement_No;
	
	@FindBy(id="rdo_17_2")
	public WebElement rdousingfundsfromyourexistingpolicies_No;
	@FindBy(id="rdo_18_2")
	public WebElement rdoterminatingexistingPolicy_No;
	@FindBy(id="flda_67")
	public WebElement Existingpolicydetail;	
	@FindBy(id="rdo_69_2")
	public WebElement Noticereadaloud_no;
	@FindBy(id="rdo_63_1")
	public WebElement Policyreplaced;
	@FindBy(id="rdo_63_2")
	public WebElement Usedasfinancing;
	
	
	@FindBy(id="rdo_28_2")
	public WebElement rdoPolicyReplacing_No;
	
	@FindBy(id="grdx40_addRowButton")
	public WebElement btnClickAdd;
	
	@FindBy(id="lb_42")
	public WebElement dropdownInsuringCompanyName;
	
	@FindBy(id="lb_36")
	public WebElement dropdownTypeInsurance;
	
	@FindBy(id="flda_40")
	public WebElement txtPolicyNumber;
	
	@FindBy(id="flda_25")
	public WebElement homeofficelocation;
	
	@FindBy(id="flda_6")
	public WebElement txtAmountBenefit;
	
	@FindBy(id="flda_64")
	public WebElement txtYearIssued;
	
	@FindBy(id="rdo_33_1")
	public WebElement rdoBeingReplaced_Yes;
	
	@FindBy(id="rdo_33_2")
	public WebElement rdoBeingReplaced_No;
	
	
	@FindBy(id="rdo_56_1")
	public WebElement rdo1035Exchange_Yes;
	
	@FindBy(id="rdo_56_2")
	public WebElement rdo1035Exchange_No;
	
	@FindBy(id="rdo_63_1")
	public WebElement rdoPolicyFinancing;
	
	@FindBy(id="rdo_74_1")
	public WebElement rdo1035InitPrem_Yes;
	
	@FindBy(id="cb_66")
	public WebElement cb1stCheckbox;
	
	@FindBy(id="rdo_46_3")
	public WebElement rdoPolicyRequired;
	
	
	@FindBy(id="rdo_28_2")
	public WebElement rdoQuestionNo_2;
	
	@FindBy(id="rdo_66_2")
	public WebElement rdoQuestionNo_3;
	
	@FindBy(id="rdo_58_2")
	public WebElement rdoQuestionNo_4;
	
	@FindBy(id="flda_56")
	public WebElement txtTotalAmountGuardian1;
	
	@FindBy(id="flda_73")
	public WebElement txtTotalAmountGuardian2;
	
	@FindBy(id="flda_52")
	public WebElement txtAgeAmount;
	
	@FindBy(id="flda_50")
	public WebElement txtProvideDetails;
	
	@FindBy(id="btn_14")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public ExistingInsurancePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
