package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuestionsChildrenPage extends GenericFunction {
	
	public HealthQuestionsChildrenPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
		@FindBy(id="CossScreenFrame")
		public WebElement frameHealthQuestion_child;
		
		@FindBy(id="rdo_27_1")
		public WebElement rdoQuestionNo_1_yes;
		
		@FindBy(id="rdo_27_2")
		public WebElement rdoQuestionNo_1_no;
		
		@FindBy(id="rdo_28_1")
		public WebElement rdoQuestionNo_2_yes;
		
		@FindBy(id="rdo_28_2")
		public WebElement rdoQuestionNo_2_no;
		
		@FindBy(id="rdo_29_1")
		public WebElement rdoQuestionNo_3_yes;
		
		@FindBy(id="rdo_29_2")
		public WebElement rdoQuestionNo_3_no;
		
		@FindBy(id="rdo_36_1")
		public WebElement rdoQuestionNo_4_yes;
		
		@FindBy(id="rdo_36_2")
		public WebElement rdoQuestionNo_4_no;
		
		@FindBy(id="rdo_33_1")
		public WebElement rdoQuestionNo_5_yes;
		
		@FindBy(id="rdo_33_2")
		public WebElement rdoQuestionNo_5_no;
		
		@FindBy(id="rdo_35_1")
		public WebElement rdoQuestionNo_6_yes;
		
		@FindBy(id="rdo_35_2")
		public WebElement rdoQuestionNo_6_no;
		
		@FindBy(id="rdo_34_1")
		public WebElement rdoQuestionNo_7_yes;
		
		@FindBy(id="rdo_34_2")
		public WebElement rdoQuestionNo_7_no;
		
		@FindBy(id="rdo_39_1")
		public WebElement rdoQuestionNo_8_yes;
		
		@FindBy(id="rdo_39_2")
		public WebElement rdoQuestionNo_8_no;
		
		@FindBy(id="rdo_41_1")
		public WebElement rdoQuestionNo_9_yes;
		
		@FindBy(id="rdo_41_2")
		public WebElement rdoQuestionNo_9_no;
		
		@FindBy(id="rdo_44_1")
		public WebElement rdoQuestionNo_10_yes;
		
		@FindBy(id="rdo_44_2")
		public WebElement rdoQuestionNo_10_no;
		
		@FindBy(id="rdo_46_1")
		public WebElement rdoQuestionNo_11_yes;
		
		@FindBy(id="rdo_46_2")
		public WebElement rdoQuestionNo_11_no;
		
		@FindBy(id="rdo_17_1")
		public WebElement rdoQuestionNo_12_yes;
		
		@FindBy(id="rdo_17_2")
		public WebElement rdoQuestionNo_12_no;
		
		@FindBy(id="rdo_18_1")
		public WebElement rdoQuestionNo_13_yes;
		
		@FindBy(id="rdo_18_2")
		public WebElement rdoQuestionNo_13_no;
		
		@FindBy(id="rdo_19_1")
		public WebElement rdoQuestionNo_14_yes;
		
		@FindBy(id="rdo_19_2")
		public WebElement rdoQuestionNo_14_no;
		
		@FindBy(id="rdo_20_1")
		public WebElement rdoQuestionNo_15_yes;
		
		@FindBy(id="rdo_20_2")
		public WebElement rdoQuestionNo_15_no;
		
		@FindBy(id="rdo_23_1")
		public WebElement rdoQuestionNo_16_yes;
		
		@FindBy(id="rdo_23_2")
		public WebElement rdoQuestionNo_16_no;
		
		@FindBy(id="rdo_8_1")
		public WebElement rdoQuestionNo_17_yes;
		
		@FindBy(id="rdo_8_2")
		public WebElement rdoQuestionNo_17_no;
		
		@FindBy(id="rdo_9_1")
		public WebElement rdoQuestionNo_18_yes;
		
		@FindBy(id="rdo_9_2")
		public WebElement rdoQuestionNo_18_no;
		
		@FindBy(id="rdo_10_1")
		public WebElement rdoQuestionNo_19_yes;
		
		@FindBy(id="rdo_10_2")
		public WebElement rdoQuestionNo_19_no;
		
		@FindBy(id="flda_1")
		public WebElement textfield;
		
		@FindBy(id="btn_11")
		public WebElement btnNext;	

}
