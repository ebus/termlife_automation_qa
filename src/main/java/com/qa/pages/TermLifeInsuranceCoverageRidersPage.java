package com.qa.pages;

import com.qa.util.GenericFunction;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TermLifeInsuranceCoverageRidersPage extends GenericFunction {
	
	@FindBy(id="CossScreenFrame")
	public WebElement TermLifeInsuranceCoverageRiders;
	@FindBy(id="flda_20")
	public WebElement faceAmount;
	@FindBy(id="lb_17")
	public WebElement term;
	@FindBy(id="rdo_35_1")
	public WebElement cashwithapp_yes;
	@FindBy(id="rdo_35_2")
	public WebElement cashwithapp_no;
	@FindBy(id="flda_38")
	public WebElement premiumAmount;
	@FindBy(id="btn_23")
	public WebElement btnNext;
	@FindBy(id="cb_16")
	public WebElement wpd_Rider;
	@FindBy(id="rdo_1_1")
	public WebElement waiverconversion_yes;
	@FindBy(id="rdo_1_2")
	public WebElement waiverconversion_no;
	@FindBy(id="cb_15")
	public WebElement children_benefit_Rider;
	@FindBy(id="lb_14")
	public WebElement unit;
	@FindBy(id="cb_10")
	public WebElement other;
	@FindBy(id="flda_9")
	public WebElement other_value;
	@FindBy(id="rdo_27_1")
	public WebElement Compltformflag_yes;
	@FindBy(id="rdo_27_2")
	public WebElement Compltformflag_no;
	@FindBy(id="rdo_33_1")
	public WebElement concurrent_coverage_yes;
	@FindBy(id="rdo_33_2")
	public WebElement concurrent_coverage_no;
	@FindBy(id="rdo_34_1")
	public WebElement amount_exceed_yes;
	@FindBy(id="rdo_34_2")
	public WebElement amount_exceed_no;
	
	
	//Initializing the Page Objects:
		public TermLifeInsuranceCoverageRidersPage() {
			super();
			PageFactory.initElements(driver, this);
		}
}

