package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ThirdPartyOptionPage extends GenericFunction{
	
	//Initializing the Page Objects:
	public ThirdPartyOptionPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameThirdPartyOption;
	
	@FindBy(id="rdo_4_2")
	public WebElement rdo_IWould;
	
	@FindBy(id="rdo_30_2")
	public WebElement rdo_IWould_Maine;
	
	@FindBy(id="flda_26")
	public WebElement txtFirst;
	
	@FindBy(id="flda_28")
	public WebElement txtLast;
	
	@FindBy(id="flda_7")
	public WebElement txtAddress;
	
	@FindBy(id="flda_12")
	public WebElement txtCity;
	
	@FindBy(id="lb_10")
	public WebElement dropdownState;
	
	@FindBy(id="flda_8")
	public WebElement txtZIP;
	
	@FindBy(id="btn_15")
	public WebElement btnNext;
	

}
